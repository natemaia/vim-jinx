" ------------------- jinx ---------------------- "
"    written by Nathaniel Maia, 2017 - 2019       "
" ----------------------------------------------- "

" ---------- Setup and Checks ----------- " {{{1

if exists('syntax_on') " {{{2
	highlight clear
	syntax reset
endif
let g:colors_name = 'jinx'

" color definitions {{{2

" start with an empty color dictionary
let s:jinx = {}

" NOTE: For those looking to edit the theme
"
" Color definition is done for both true color
" and 256 color. The format is in ['HEX', 256color, 16color]
"
" let s:jinx.black = ['#000000', 0, 0]

let g:terminal_ansi_colors = [
			\ '#4D545E', '#D9534F', '#80B080', '#FFEB56', '#6699CC', '#CC99CC', '#5DD5FF', '#E1E1E1',
			\ '#4D545E', '#D9534F', '#80B080', '#FFEB56', '#6699CC', '#CC99CC', '#5DD5FF', '#E1E1E1'
			\ ]
let s:i = 0
for s:color in g:terminal_ansi_colors
	let g:terminal_color_{s:i} = s:color
	let s:i += 1
endfor

let s:jinx.red    = ['#EE5555', 210,  1]
let s:jinx.green  = ['#88BB88', 108,  2]
let s:jinx.yellow = ['#FFCC66', 220,  3]
let s:jinx.blue   = ['#4488CC',  32,  4]
let s:jinx.purple = ['#AA88CC', 140,  5]
let s:jinx.cyan   = ['#44CCEE',  81,  6]
let s:jinx.orange = ['#DE753A', 172, 11]

if exists('g:jinx_theme') && g:jinx_theme =~? 'day'
	set background=light
	let s:jinx.fgr     = ['#494949', 238,  0]
	let s:jinx.bgr     = ['#FFFFFF', 254,  7]
	let s:jinx.line    = ['#D0D0D0', 252,  7]
	let s:jinx.comment = ['#808080', 244, 15]
	let s:jinx.yellow  = ['#FFA500', 178,  3] " due to how bright light is we need some contrast
	let s:jinx.green   = ['#0B6623', 108,  2] " colours for yellow, green, purple, and blue.
	let s:jinx.purple  = ['#800080', 140,  5]
	let s:jinx.blue    = ['#003366',  32,  4]
	let s:jinx.menubgr = ['#AAAAAA', 248, 15]
elseif exists('g:jinx_theme') && g:jinx_theme =~? 'midnight'
	set background=dark
	let s:jinx.fgr     = ['#CCCCCC', 251, 15]
	let s:jinx.bgr     = ['#111111', 234,  0]
	let s:jinx.line    = ['#2A2A2F', 237,  8]
	let s:jinx.comment = ['#777777', 243,  7]
	let s:jinx.menubgr = ['#212121', 238,  0]
else " night
	set background=dark
	let s:jinx.fgr     = ['#E1E1E1', 254, 15]
	let s:jinx.bgr     = ['#4D545E', 238,  0]
	let s:jinx.line    = ['#5F6772', 243,  8]
	let s:jinx.comment = ['#B9B9B9', 250,  7]
	let s:jinx.menubgr = ['#30343E', 237,  0]
endif

if $TERM =~? 'linux' || &t_Co < 256  " {{{2
	function! <SID>hlg(GRP, FG, BG, ATT) abort
		if a:FG !=# ''
			let l:fg_col = get(s:jinx, a:FG)
			exec 'highlight! '.a:GRP.' guifg='.l:fg_col[0].' ctermfg='.l:fg_col[2]
		endif
		if a:BG !=# ''
			let l:bg_col = get(s:jinx, a:BG)
			exec 'highlight! '.a:GRP.' guibg='.l:bg_col[0].' ctermbg='.l:bg_col[2]
		endif
		if a:ATT !=# ''
			exec 'highlight! '.a:GRP.' gui='.a:ATT.' cterm='.a:ATT
		endif
	endfunction
else
	function! <SID>hlg(GRP, FG, BG, ATT) abort
		if a:FG !=# ''
			let l:fg_col = get(s:jinx, a:FG)
			exec 'highlight! '.a:GRP.' guifg='.l:fg_col[0].' ctermfg='.l:fg_col[1]
		endif
		if a:BG !=# ''
			let l:bg_col = get(s:jinx, a:BG)
			exec 'highlight! '.a:GRP.' guibg='.l:bg_col[0].' ctermbg='.l:bg_col[1]
		endif
		if a:ATT !=# ''
			exec 'highlight! '.a:GRP.' gui='.a:ATT.' cterm='.a:ATT
		endif
	endfunction
endif

" ---------- Highlight Groups ----------- " {{{1

" Editor {{{2

if $TERM == 'linux'
	call <SID>hlg('Visual', 'bgr', 'fgr', '')
else
	call <SID>hlg('Visual', '', 'line', '')
endif

call <SID>hlg('Title',        'comment',  '',         'bold')
call <SID>hlg('SignColumn',   '',         'line',         '')
call <SID>hlg('CursorLine',   '',         'line',         '')
call <SID>hlg('CursorColumn', '',         'line',         '')
call <SID>hlg('CursorLineNr', 'cyan',     'line',         '')
call <SID>hlg('LineNr',       'comment',  'line',         '')
call <SID>hlg('ColorColumn',  'fgr',      'red',          '')
call <SID>hlg('Error',        'red',      'bgr',          '')
call <SID>hlg('ErrorMsg',     'red',      'bgr',          '')
call <SID>hlg('WarningMsg',   'red',      '',             '')
call <SID>hlg('MatchParen',   'cyan',     'bgr', 'underline')
call <SID>hlg('ModeMsg',      'cyan',     '',             '')
call <SID>hlg('MoreMsg',      'cyan',     '',             '')
call <SID>hlg('Directory',    'blue',     '',             '')
call <SID>hlg('Question',     'green',    '',             '')
call <SID>hlg('NonText',      'comment',  '',             '')
call <SID>hlg('SpecialKey',   'comment',  '',             '')
call <SID>hlg('Folded',       'comment',  'line',         '')
call <SID>hlg('Search',       'bgr',      'fgr',          '')
call <SID>hlg('IncSearch',    'fgr',      'bgr',          '')
call <SID>hlg('HLNext',       'bgr',      'red',          '')
call <SID>hlg('Normal',       'fgr',      '',             '')
call <SID>hlg('NormalFloat',  'fgr',      '',             '')
call <SID>hlg('VertSplit',    'line',     'comment',      '')
call <SID>hlg('FloatBorder',  'red',      'bgr',          '')

" Tabline {{{2

call <SID>hlg('TabLine',      'fgr',     'line',    '')
call <SID>hlg('TabLineFill',  'line',    'line',    '')
call <SID>hlg('TabLineSel',   'line',    'green',   '')
call <SID>hlg('WildMenu',     'bgr',     'fgr',     '')
call <SID>hlg('Pmenu',        'comment', 'menubgr', '')
call <SID>hlg('PmenuSel',     'bgr',     'comment', '')
call <SID>hlg('PmenuSbar',    'comment', 'menubgr', '')
call <SID>hlg('PmenuThumb',   'comment', 'menubgr', '')
call <SID>hlg('StatusLine',   'line',    'fgr',     '')
call <SID>hlg('StatusLineNC', 'fgr',     'line',    '')

" Spelling {{{2
call <SID>hlg('SpellBad',   '', '', 'underline')
call <SID>hlg('SpellLocal', '', '', 'underline')
call <SID>hlg('SpellRare',  '', '', 'underline')

" Generic {{{2

call <SID>hlg('Comment',      'comment', '',     '')
call <SID>hlg('Todo',         'red',     '',     '')
call <SID>hlg('Exception',    'red',     '',     '')
call <SID>hlg('Float',        'cyan',    '',     '')
call <SID>hlg('Number',       'cyan',    '',     '')
call <SID>hlg('Include',      'cyan',    '',     '')
call <SID>hlg('Character',    'blue',    '',     '')
call <SID>hlg('Operator',     'blue',    '',     '')
call <SID>hlg('String',       'blue',    '',     '')
call <SID>hlg('Label',        'green',   '',     '')
call <SID>hlg('Repeat',       'purple',  '',     '')
call <SID>hlg('Statement',    'green',   '',     '')
call <SID>hlg('Conditional',  'green',   '',     '')
call <SID>hlg('Boolean',      'green',   '',     '')
call <SID>hlg('Keyword',      'green',   '',     '')
call <SID>hlg('Macro',        'purple',  '',     '')
call <SID>hlg('Define',       'purple',  '',     '')
call <SID>hlg('Special',      'purple',  '',     '')
call <SID>hlg('Tag',          'purple',  '',     '')
call <SID>hlg('Type',         'yellow',  '', 'none')
call <SID>hlg('TypeDef',      'purple',  '',     '')
call <SID>hlg('Structure',    'purple',  '',     '')
call <SID>hlg('StorageClass', 'purple',  '',     '')
call <SID>hlg('PreProc',      'yellow',  '',     '')
call <SID>hlg('Constant',     'yellow',  '',     '')
call <SID>hlg('Identifier',   'yellow',  '',     '')
call <SID>hlg('PreCondit',    'yellow',  '',     '')
call <SID>hlg('Conceal',      'orange',  '',     '')
call <SID>hlg('Function',     'orange',  '',     '')

" -------------- Cleanup ---------------- " {{{1

delfunction <SID>hlg

" vim:fdm=marker
