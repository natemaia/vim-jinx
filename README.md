# Jinx
A Vim color scheme, made to be eye catchy while still maintaining visibility, and base 0-7 colours.

Inspired by themes like Tomorrow, Dracula, Atom, and Spacemacs, but with my own twists.

### Installation
---
Vim Plug *(recommended)*
```
Plug 'https://bitbucket.org/natemaia/vim-jinx'
```

Pathogen
```
git clone https://bitbucket.org/natemaia/vim-jinx ~/.vim/bundle/vim-jinx
```

Vim
```
mkdir ~/.vim/pack/plugins/start
git clone https://bitbucket.org/natemaia/vim-jinx ~/.vim/pack/plugins/start/vim-jinx
```

### Configuration
---
- Set variant *(night, day, or midnight)*
```
let g:jinx_theme = 'night'
```

- Enable `termguicolors` to enable true colors *(terminal dependant)*
```
set termguicolors
```

- Set the `colorscheme`
```
colorscheme jinx
```

### Screenshots
![Image](https://i.imgur.com/RB63MAa.png)
![Image](https://i.imgur.com/07jwM4g.png)
![Image](https://cdn.scrot.moe/images/2018/12/28/2018-12-27-230438_510x499_scrot.png)

